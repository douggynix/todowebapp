**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**
This project is built upon Maven.
You can compile, package the war and run it under Tomee Application server with this simple command:

**mvn compile package tomee:run**