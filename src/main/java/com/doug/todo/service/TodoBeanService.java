package com.doug.todo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.doug.todo.contract.TodoContract;
import com.doug.todo.model.Todo;


@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@LocalBean
public class TodoBeanService implements TodoContract{
	//use copy on write ArrayList to ensure thread safety upon concurrent access
	private List<Todo> todoList = new  CopyOnWriteArrayList<Todo>();
	
	

	public Todo createTodo(String title,String description,boolean isComplete) throws Exception{
		Todo todo = new Todo();
		String id = UUID.randomUUID().toString();
		todo.setId(id);
		todo.setTitle(title);
		todo.setDescription(description);
		todo.setCompleted(isComplete);
		this.add(todo);
		return todo;
	}
	
	public List<Todo> retrieveList(){
		return this.todoList;
	}
	
	public Todo retrieveTodo(String id){
		List<Todo> todos = this.retrieveList();
		
		List<Todo> filterList= todos.stream().filter( t  -> t.getId().equalsIgnoreCase(id) ).collect(Collectors.toList());
		if(filterList.size() != 0){
			return filterList.get(0);
		}
		else{
			return null;
		}
		
	}
	
	public void add(Todo task) throws Exception {
		// TODO Auto-generated method stub
		if(task == null || task.getId() == null ){
			throw new IllegalStateException("Cannot Add Todo. Todo is null or Todo ID is null");
		}
		
		
		if(this.todoList.contains(task)){
			throw new IllegalStateException("Cannot Add Task. Task already exists");
		}
		
		this.todoList.add(task);
	}

	public Todo update(String id, String title, String description, boolean completed) throws Exception {
		// TODO Auto-generated method stub
		
		Todo todo=null;
		if(id!=null){
			todo = this.retrieveTodo(id.toLowerCase().trim());
		}
		
		if(todo!=null){
			System.out.println("Todo Item '"+id+"' found!");
			if(title!=null){
				todo.setTitle(title);
			}
			if(description!=null){
				todo.setDescription(description);
			}
			todo.setCompleted(completed);
		}
		else{
			System.out.println("No Todo Item found for ID '"+id+"'");
		}
		
		
		
		return todo;
	}



	@Override
	public boolean delete(String id) throws Exception {
		// TODO Auto-generated method stub
		boolean status=false;
		if(id!=null){
			status=this.todoList.removeIf(t -> t.getId().equalsIgnoreCase(id));
		}
		return status;
	}
	

}
