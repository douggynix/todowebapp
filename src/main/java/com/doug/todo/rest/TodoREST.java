package com.doug.todo.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.doug.todo.model.Todo;
import com.doug.todo.service.TodoBeanService;

@Path("/todo")
public class TodoREST {
	
	@EJB
	private TodoBeanService todoService;
	
	@GET
	@Path("list")
	public Response getTodoList(@QueryParam("id") String id){
		
		if(id != null && !id.isEmpty()){
			Todo todo = todoService.retrieveTodo(id.toLowerCase().trim());
			System.out.println("ID : "+id);
			if(todo!=null){
				return Response.ok(todo,MediaType.APPLICATION_JSON).build();
			}
			else{
				return Response.status(Status.NOT_FOUND).entity("Todo Item with ID " + id + " doesn't Exist. Verify Todo Item ID").build();
			}
		}
		else{
		List<Todo> todos = this.todoService.retrieveList();
		//this is needed for jarx-rs implementations that doesn't handle generic type for list serialization for jaxb
		//use Generic Entity
		GenericEntity<List<Todo>> list = new GenericEntity<List<Todo>>(todos){}; 
		
		return Response.ok(list,MediaType.APPLICATION_JSON).build();
		}
	}
	
	
	@POST
	@Path("create")
	public Response createTodo(@QueryParam("title")			 @DefaultValue("Unknown Title") 	String title,
							   @QueryParam("description")	 @DefaultValue("No Description") 	String description ){
		
		Todo todo=null;
		try {
			todo = todoService.createTodo(title, description, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.ok(todo,MediaType.APPLICATION_JSON).build();
	}
	
	@POST
	@Path("update")
	public Response updateTodo(@QueryParam("id") String id,
						   @QueryParam("title") 	String title,
			   			   @QueryParam("description")	String description,
			   			   @QueryParam("completed") String completed) {
		System.out.println("Update Param : id="+id+" title="+title+" description="+description+" completed="+completed);
		Todo todo=null;
		
		if(completed==null){
			return Response.status(Status.NOT_ACCEPTABLE).entity("completed field must not be empty. it is required to  set its value to true or false").build();
		}
		
		boolean isComplete=false;
		try{
			isComplete= Boolean.parseBoolean(completed.toLowerCase());
		}
		catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.NOT_ACCEPTABLE).entity("Invalid value for field completed : '" + completed +"'. Accept true or false").build();
		}
		
		try {
			todo = todoService.update(id.toLowerCase(), title, description, isComplete);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Status.EXPECTATION_FAILED).entity("Todo Item Update for ID "+id+" may have failed. Error : "+e.getMessage()).build();
		}

		if(todo!=null){
			return Response.ok(todo,MediaType.APPLICATION_JSON).build(); 
		}
		else{
			return Response.status(Status.NOT_FOUND).entity("Todo Item with ID " + id + " doesn't Exist. Verify Todo Item ID").build();
		}
	}
	
	
	@DELETE
	@Path("delete/{id}")
	public Response removeTodo(@PathParam("id") String id){
		boolean status=false;
		try {
			System.out.println("Atempt to delete Todo Item ID " + id);
			 status = todoService.delete(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(status){
			return Response.ok("Todo Item "+id+" has been removed",MediaType.TEXT_PLAIN).build();
		}
		else{
			return Response.status(Status.NOT_FOUND).entity("No Todo Item with ID " + id + " has been found or removed").build();
		}
		
	}

}
