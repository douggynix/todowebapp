package com.doug.todo.contract;

import com.doug.todo.model.Todo;

public interface TodoContract {
	
	public void add(Todo task) throws Exception;
	
	public Todo update(String id, String title, String description, boolean completed ) throws Exception;
	
	public boolean delete(String id) throws Exception;
	

}
